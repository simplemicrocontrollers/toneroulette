# toneroulette

Educational game for children: "Guess The Note" based on Arudino, buzzer, RGB LED, servo and rotary encoder

![toneroulette](https://dev.ussr.win/microcontrollers/toneroulette/raw/branch/master/toneroulette_agg.jpg)

![toneroulette](https://dev.ussr.win/microcontrollers/toneroulette/raw/branch/master/toneroulette_bb.jpg)

